function HsvToRgb(h, s, v) {
  //https://www.rapidtables.com/convert/color/hsv-to-rgb.html
  //h in range 0-360
  //s in range 0-1
  //v in range 0-1
  var c = v * s;
  var x = c * (1.0 - Math.abs(((h / 60.0) % 2) - 1.0));
  var m = v - c;
  var z;
  if (h < 60.0)
    z = [c, x, 0];
  else if (h < 120)
    z = [x, c, 0];
  else if (h < 180)
    z = [0, c, x];
  else if (h < 240)
    z = [0, x, c];
  else if (h < 300)
    z = [x, 0, c];
  else
    z = [c, 0, x];
  var col = [(z[0] + m) * 255.0, (z[1] + m) * 255.0, (z[2] + m) * 255.0]
  return col
}

function RgbToHsv(r, g, b) {
  //https://www.rapidtables.com/convert/color/rgb-to-hsv.html
  var rgb2 = [r / 255.0, g / 255.0, b / 255.0];
  var cmax = Math.max.apply(Math, rgb2);
  var cmin = Math.min.apply(Math, rgb2);
  var delta = cmax - cmin;
  var argmax = rgb2.indexOf(cmax);

  var hue = 0.0;
  if (delta > 0.0) {
    if (argmax == 0)
      hue = 60.0 * (((rgb2[1] - rgb2[2]) / delta) % 6);
    else if (argmax == 1)
      hue = 60.0 * (((rgb2[2] - rgb2[0]) / delta) + 2);
    else
      hue = 60.0 * (((rgb2[0] - rgb2[1]) / delta) + 4);
  }

  var saturation = 0.0;
  if (cmax > 0.0)
    saturation = delta / cmax;
  var value = cmax;

  return [hue, saturation, value];
}

// ***********************************************************************

function RefreshSampleColour(r, g, b, a, hexStr) {
  $('#col-picker-display').css("background-color", hexStr);
}

// ***********************************************************************

function RgbaToHex(r, g, b, a)
{
  var hexStr = (Math.round(r).toString(16).padStart(2, '0') +
    Math.round(g).toString(16).padStart(2, '0') +
    Math.round(b).toString(16).padStart(2, '0') +
    Math.round(Math.round(a * 255.0)).toString(16).padStart(2, '0'));
  return hexStr;
}

function RefreshRgbPicker() {
  var hue = parseFloat($('#col-picker-shade2').data("hue"));
  var saturation = parseFloat($('#col-picker-shade2').data("saturation"));
  var value = parseFloat($('#col-picker-shade2').data("value"));
  var alpha = parseFloat($('#col-picker-shade2').data("alpha"));

  var rgbCol = HsvToRgb(hue, saturation, value);

  $('#col-picker-r-text').val(Math.round(rgbCol[0]));
  $('#col-picker-g-text').val(Math.round(rgbCol[1]));
  $('#col-picker-b-text').val(Math.round(rgbCol[2]));
  $('#col-picker-a-text').val(alpha.toFixed(2));
}

function RefreshHsvPicker() {
  var hue = parseFloat($('#col-picker-shade2').data("hue"));
  var saturation = parseFloat($('#col-picker-shade2').data("saturation"));
  var value = parseFloat($('#col-picker-shade2').data("value"));

  $('#col-picker-h-text').val(Math.round(hue));
  $('#col-picker-s-text').val(saturation.toFixed(2));
  $('#col-picker-v-text').val(value.toFixed(2));
}

function RefreshHtmlPicker() {
  var hue = parseFloat($('#col-picker-shade2').data("hue"));
  var saturation = parseFloat($('#col-picker-shade2').data("saturation"));
  var value = parseFloat($('#col-picker-shade2').data("value"));
  var alpha = parseFloat($('#col-picker-shade2').data("alpha"));

  var rgbCol = HsvToRgb(hue, saturation, value);

  var hexStr = (Math.round(rgbCol[0]).toString(16).padStart(2, '0') +
    Math.round(rgbCol[1]).toString(16).padStart(2, '0') +
    Math.round(rgbCol[2]).toString(16).padStart(2, '0') +
    Math.round(Math.round(alpha * 255.0)).toString(16).padStart(2, '0'));
  $('#col-picker-html-text').val(hexStr);
}

function RefreshOutputColour(updateGradient = true) {
  var hue = parseFloat($('#col-picker-shade2').data("hue"));
  var saturation = parseFloat($('#col-picker-shade2').data("saturation"));
  var value = parseFloat($('#col-picker-shade2').data("value"));
  var alpha = parseFloat($('#col-picker-shade2').data("alpha"));

  var rgbCol = HsvToRgb(hue, saturation, value);

  var hexStr = RgbaToHex(rgbCol[0], rgbCol[1], rgbCol[2], alpha);
  var hexStr2 = "#" + hexStr;
  RefreshSampleColour(rgbCol[0], rgbCol[1], rgbCol[2], alpha, hexStr2);
  if (updateGradient)
    RefreshBuilderTabColour(activeTab, rgbCol[0], rgbCol[1], rgbCol[2], alpha, hexStr2);
}

function RefreshShadePicker() {
  var saturation = parseFloat($('#col-picker-shade2').data("saturation"));
  var value = parseFloat($('#col-picker-shade2').data("value"));
  var hue = parseFloat($('#col-picker-shade2').data("hue"));

  $('#col-picker-shade2').css("background-image", "linear-gradient(to right, white, hsl(" + hue.toString() + ", 100%, 50%))");

  $('#col-picker-shade-marker').css('left', saturation * $('#col-picker-shade2').width() - 3);
  $('#col-picker-shade-marker').css('top', (1.0 - value) * $('#col-picker-shade2').height() - 3);
}

function RefreshHuePicker() {
  var hue = parseFloat($('#col-picker-shade2').data("hue"));

  $('#col-picker-hue-marker').css('top', hue / 360.0 * $('#col-picker-hue').height() - 3);
}

function RefreshAlphaPicker() {
  var alpha = parseFloat($('#col-picker-shade2').data("alpha"));

  $('#col-picker-alpha-marker').css('top', (1.0 - alpha) * $('#col-picker-alpha').height() - 3);
}

$('#col-picker-hue').click(function(e) {

  var yPos = e.pageY - $(this).offset().top;
  var hue = 360.0 * yPos / $(this).height();

  $('#col-picker-shade2').data("hue", hue);

  RefreshShadePicker();
  RefreshHuePicker();
  RefreshRgbPicker();
  RefreshHsvPicker();
  RefreshHtmlPicker();
  RefreshOutputColour();

  e.stopPropagation();
  e.preventDefault();
  return false;
});

$('#col-picker-shade2').click(function(e) {

  var xPos = e.pageX - $(this).offset().left;
  var yPos = e.pageY - $(this).offset().top;
  var saturation = xPos / $(this).width();
  var value = 1.0 - yPos / $(this).height();
  $(this).data("saturation", saturation);
  $(this).data("value", value);

  RefreshShadePicker();
  RefreshRgbPicker();
  RefreshHsvPicker();
  RefreshHtmlPicker();
  RefreshOutputColour();

  e.stopPropagation();
  e.preventDefault();
  return false;
});

$('#col-picker-alpha2').click(function(e) {

  var yPos = e.pageY - $(this).offset().top;
  var alpha = 1.0 - yPos / $(this).height();

  $('#col-picker-shade2').data("alpha", alpha);

  RefreshRgbPicker();
  RefreshAlphaPicker();
  RefreshHtmlPicker();
  RefreshOutputColour();

  e.stopPropagation();
  e.preventDefault();
  return false;
});

function RgbValuesChanged() {
  var r = parseFloat($('#col-picker-r-text').val());
  var g = parseFloat($('#col-picker-g-text').val());
  var b = parseFloat($('#col-picker-b-text').val());
  var a = parseFloat($('#col-picker-a-text').val());
  if (isNaN(r)) r = 0.0;
  if (isNaN(g)) g = 0.0;
  if (isNaN(b)) b = 0.0;
  if (isNaN(a)) a = 1.0;
  if (r < 0.0) r = 0.0;
  if (g < 0.0) g = 0.0;
  if (b < 0.0) b = 0.0;
  if (a < 0.0) a = 0.0;
  if (r > 255.0) r = 255.0;
  if (g > 255.0) g = 255.0;
  if (b > 255.0) b = 255.0;
  if (a > 1.0) a = 1.0;

  var hsv = RgbToHsv(r, g, b);

  $('#col-picker-shade2').data("hue", hsv[0]);
  $('#col-picker-shade2').data("saturation", hsv[1]);
  $('#col-picker-shade2').data("value", hsv[2]);
  $('#col-picker-shade2').data("alpha", a);

  RefreshShadePicker();
  RefreshHuePicker();
  RefreshHsvPicker();
  RefreshAlphaPicker();
  RefreshHtmlPicker();
  RefreshOutputColour();
}

$('#col-picker-r-text').on('input', function(e) {
  RgbValuesChanged();
});

$('#col-picker-g-text').on('input', function(e) {
  RgbValuesChanged();
});

$('#col-picker-b-text').on('input', function(e) {
  RgbValuesChanged();
});

$('#col-picker-a-text').on('input', function(e) {
  RgbValuesChanged();
});

function HsvValuesChanged() {
  var h = parseFloat($('#col-picker-h-text').val());
  var s = parseFloat($('#col-picker-s-text').val());
  var v = parseFloat($('#col-picker-v-text').val());
  if (isNaN(h)) h = 0.0;
  if (isNaN(s)) s = 0.0;
  if (isNaN(v)) v = 0.0;
  if (h < 0.0) h = 0.0;
  if (s < 0.0) s = 0.0;
  if (v < 0.0) v = 0.0;
  if (h > 360.0) h = 360.0;
  if (s > 1.0) s = 1.0;
  if (v > 1.0) v = 1.0;

  $('#col-picker-shade2').data("hue", h);
  $('#col-picker-shade2').data("saturation", s);
  $('#col-picker-shade2').data("value", v);

  RefreshShadePicker();
  RefreshHuePicker();
  RefreshRgbPicker();
  RefreshHtmlPicker();
  RefreshOutputColour();
}

$('#col-picker-h-text').on('input', function(e) {
  HsvValuesChanged();
});

$('#col-picker-s-text').on('input', function(e) {
  HsvValuesChanged();
});

$('#col-picker-v-text').on('input', function(e) {
  HsvValuesChanged();
});

function HtmlValuesChanged() {
  var htmlColStr = $('#col-picker-html-text').val();
  var htmlCol = parseInt(htmlColStr, 16);
  if (isNaN(htmlCol)) htmlCol = 0;
  var a, b, g, r;
  if (htmlColStr.length == 8) {
    a = htmlCol & 0xff;
    b = (htmlCol >> 8) & 0xff;
    g = (htmlCol >> 16) & 0xff;
    r = (htmlCol >> 24) & 0xff;
  } else if (htmlColStr.length == 6) {
    a = 0xff;
    b = htmlCol & 0xff;
    g = (htmlCol >> 8) & 0xff;
    r = (htmlCol >> 16) & 0xff;
  } else
    return;

  var hsv = RgbToHsv(r, g, b);

  $('#col-picker-shade2').data("hue", hsv[0]);
  $('#col-picker-shade2').data("saturation", hsv[1]);
  $('#col-picker-shade2').data("value", hsv[2]);
  $('#col-picker-shade2').data("alpha", a / 255.0);

  RefreshShadePicker();
  RefreshHuePicker();
  RefreshRgbPicker();
  RefreshHsvPicker();
  RefreshAlphaPicker();
  RefreshOutputColour();
}

$('#col-picker-html-text').on('input', function(e) {
  HtmlValuesChanged();
});

function ColourPickerSet(r, g, b, a = 1.0, updateGradient = true) {
  var hsv = RgbToHsv(r, g, b);

  $('#col-picker-shade2').data("hue", hsv[0]);
  $('#col-picker-shade2').data("saturation", hsv[1]);
  $('#col-picker-shade2').data("value", hsv[2]);
  $('#col-picker-shade2').data("alpha", a);

  RefreshShadePicker();
  RefreshHuePicker();
  RefreshRgbPicker();
  RefreshHsvPicker();
  RefreshAlphaPicker();
  RefreshHtmlPicker();
  RefreshOutputColour(updateGradient);
}

// *******************************************************************************

var activeTab = $('#gradient-builder-tab-left');
var dragged;
var dragIsInside = true;

function GradientBuilderTabClicked(obj, e) {

  var colour = obj.data("colour").split(",");
  ColourPickerSet(colour[0], colour[1], colour[2], colour[3], false);
  activeTab = obj;
}

$('.gradient-builder-tab').click(function(e) {

  GradientBuilderTabClicked($(this), e);

  e.stopPropagation();
  e.preventDefault();
  return false;
});

function GradientBuilderAddTab(xPercent, r=255, g=255, b=255, a=1.0)
{
  var el = $('<div class="gradient-builder-tab gradient-builder-tab-draggable" draggable="true" data-colour="'+r.toString()+','+g.toString()+','+b.toString()+','+a.toString()+'" style="left: ' + xPercent.toString() + '%;"></div>');
  el.click(function(e) {
    GradientBuilderTabClicked($(this), e);

    e.stopPropagation();
    e.preventDefault();
    return false;
  });
  el.on('dragstart', function(e) {
    dragged = e.target;
  });
  el.on('dragend', function(e) {
    if (!dragIsInside)
      $(dragged).remove();

    RefreshBuilder();
  });

  var hexStr = RgbaToHex(r, g, b, a);
  var hexStr2 = "#" + hexStr;

  $('#gradient-builder').append(el);
  RefreshBuilderTabColour(el, r, g, b, a, hexStr2);

  return el;
}

$('#gradient-builder').click(function(e) {

  var xPos = e.pageX - $(this).offset().left;
  var xPercent = 100.0 * xPos / $(this).width();

  var el = GradientBuilderAddTab(xPercent);

  activeTab = el;
  ColourPickerSet(255, 255, 255, 1.0, false);

  RefreshBuilder();

  e.stopPropagation();
  e.preventDefault();
  return false;
});

$('#gradient-builder').on('dragenter', function(e) {
  dragIsInside = true;
  e.preventDefault();
});

$('#gradient-builder').on('dragover', function(e) {
  dragIsInside = true;
  e.preventDefault();
});

$('#gradient-builder').on('dragleave', function(e) {
  dragIsInside = false;
  e.preventDefault();
});

$('#gradient-builder').on('drop', function(e) {

  var draggedTag = $(dragged);
  draggedTag.css('left', (100.0 * (e.clientX - $(this).offset().left) / $(this).width()).toString() + "%");

  activeTab = draggedTag;
  GradientBuilderTabClicked(activeTab, null);

  RefreshBuilder();
  e.preventDefault();
});

function RefreshBuilderTabColour(tab, r, g, b, a, hexStr) {
  tab.data('colour', r.toString() + "," + g.toString() + "," + b.toString() + "," + a.toString());
  tab.css('background-color', hexStr);
  RefreshBuilder();
}

function RefreshBuilder() {
  var sortable = [];
  $(".gradient-builder-tab").each(function(index) {

    var left = $(this).css('left');
    checkpx = left.substr(left.length - 2, 2) == "px";
    sortable.push([parseFloat(left.substr(0, left.length - 2)), $(this).css('background-color')]);
  });
  sortable.sort(function(a, b) {
    return a[0] - b[0];
  });

  var gradCss = "linear-gradient(to right";
  sortable.forEach(function(it) {
    gradCss += " ," + it[1] + " " + (100.0 * it[0] / $('#gradient-builder').width()).toString() + "%";
  });
  gradCss += ")";

  $('#gradient-builder').css('background-image', gradCss);
}

function SetGradientBuilderState(color_list)
{
  $(".gradient-builder-tab-draggable").each(function(index) { 
    $(this).remove();
  });

  var el;
  for (i = 0; i < color_list.length; ++i) {
    var gc = color_list[i];
    if (gc['gradient_score'] == 0.0)
    {
        var hexStr = RgbaToHex(gc['r'], gc['g'], gc['b'], gc['a']/255.0);
        var hexStr2 = "#" + hexStr;
        el = $('#gradient-builder-tab-left');
        RefreshBuilderTabColour(el, gc['r'], gc['g'], gc['b'], gc['a']/255.0, hexStr2);
    }
    else if (gc['gradient_score'] == 1.0)
    {
        var hexStr = RgbaToHex(gc['r'], gc['g'], gc['b'], gc['a']/255.0);
        var hexStr2 = "#" + hexStr;
        el = $('#gradient-builder-tab-right');
        RefreshBuilderTabColour(el, gc['r'], gc['g'], gc['b'], gc['a']/255.0, hexStr2);
    }
    else
      el = GradientBuilderAddTab(gc['gradient_score']*100.0, gc['r'], gc['g'], gc['b'], gc['a']/255.0);
  }

  activeTab = el;

  RefreshBuilder();
  GradientBuilderTabClicked(activeTab, null);
}

function GetGradientBuilderState()
{ 
  var sortable = [];
  $(".gradient-builder-tab").each(function(index) {

    var left = $(this).css('left');
    checkpx = left.substr(left.length - 2, 2) == "px";
    sortable.push([parseFloat(left.substr(0, left.length - 2)), $(this).css('background-color'), $(this)]);
  });
  sortable.sort(function(a, b) {
    return a[0] - b[0];
  });

  var color_list = [];
  var i = 0;
  sortable.forEach(function(it) {
    var gradient_score = 100.0 * it[0] / $('#gradient-builder').width() / 100.0;
    if (i == 0)
      gradient_score = 0.0;
    else if (i == sortable.length - 1)
      gradient_score = 1.0;
    var colour = it[2].data("colour").split(",");
    color_list.push({'gradient_score': gradient_score, 'r': colour[0], 'g': colour[1], 'b': colour[2], 'a': colour[3]});
    i++;
  });

  return color_list;
}

